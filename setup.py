from glob import glob
from os.path import splitext, basename

from setuptools import setup, find_packages


setup(
    name='crismy',
    version='0.0.1',
    description="CRISM python library",
    long_description="",
    author="Andrew Annex",
    author_email='aannex1@jhu.edu',
    packages=find_packages('src'),
    package_dir={'': 'src'},
    py_modules=[splitext(basename(path))[0] for path in glob('src/*.py')],
    requires = ['numpy', 'pvl', 'scipy', 'rasterio>=1.1.0'],
    tests_require=["pytest"]
)