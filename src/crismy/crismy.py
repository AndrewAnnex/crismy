from pathlib import Path
import os

import pvl
import numpy as np
import pandas as pd
import rasterio as rio

from .utils import utils

class CRISM(object):

    # bad columns 0-4, -1

    def __init__(self, path):
        self.label_path = self._label_path(path)
        self.image_path = self._image_path(path)
        self.name = self.image_path.stem
        self.label = pvl.load(str(self.label_path.absolute()))
        self._pdsobj = rio.open(str(self.label_path))

    def __repr__(self):
        return f'{self.__class__.__qualname__} Observation {self.name}'

    def _label_path(self, path: str)-> Path:
        return self._get_path(str(path), 'lbl')

    def _image_path(self, path: str) -> Path:
        return self._get_path(str(path), 'img')

    @staticmethod
    def _get_path(path: str, ext: str)-> Path:
        if path.endswith(ext):
            f_path = Path(path)
        else:
            f_path = Path(path[:-3] + ext)
        if f_path.exists():
            return f_path
        else:
            raise IOError(f'File: {f_path} does not exist')

    @property
    def data(self):
        return self._pdsobj.read()

    @property
    def bands(self):
        """Number of image bands."""
        return len(self._pdsobj.indexes)

    @property
    def lines(self):
        """Number of lines per band."""
        return self._pdsobj.shape[0]

    @property
    def samples(self):
        """Number of samples per line."""
        return self._pdsobj.shape[1]

    @property
    def rows(self):
        """number of rows (aka lines) per band."""
        return self.lines

    @property
    def cols(self):
        """number of columns (aka samples) per band."""
        return self.samples

    @property
    def format(self):
        """Image format."""
        return self._pdsobj.format

    @property
    def dtype(self):
        """Pixel data type."""
        return self._pdsobj.dtypes

    @property
    def start_byte(self):
        """Index of the start of the image data (zero indexed)."""
        return self._pdsobj.start_byte

    @property
    def shape(self):
        """Tuple of images bands, lines and samples."""
        return self._pdsobj.shape

    @property
    def size(self):
        return self._pdsobj.size

    @staticmethod
    def open(path):
        """
        Dispatch based on prefix
        :param path:
        :return:
        """
        stem = Path(path).stem.lower()
        if stem.startswith('hsp'):
            return HSP(path)
        elif stem.startswith('frt'):
            return FRT(path)
        elif stem.startswith('hrl'):
            return HRL(path)
        elif stem.startswith('hrs'):
            return HRS(path)
        elif stem.startswith('frs'):
            return FRS(path)
        else:
            raise NotImplementedError(f'No object for data type {stem[0:3]}')


class HSP(CRISM):

    rnt_key  = '^ROWNUM_TABLE'
    rnto_key = 'ROWNUM_TABLE'
    hk_key = '^TRDR_HK_TABLE'

    def __init__(self, path):
        super().__init__(path)
        self.rownum_table = self._load_rownum_table(path)
        self.trdr_hk_table = self._load_hk_table(path)

    @property
    def data(self):
        """return the data with the null band and null samples(cols) removed"""
        # todo use rasterio with more brain here
        return self._pdsobj.read()[1:,:,3:-1]

    def __getitem__(self, key):
        return self.data[key]

    def _load_rownum_table(self, path):
        for _, file in (x for x in self.label if x[0] == 'FILE'):
            if self.rnt_key in file and self.rnto_key in file:
                rnt  = file[self.rnt_key]
                rnto = file[self.rnto_key]
                start_byte = rnt[1]
                rows = rnto['ROWS']
                cols = rnto['COLUMNS']
                rbytes  = rnto['ROW_BYTES']
                bitmask = rnto['COLUMN']['BIT_MASK']
                start_pos = (start_byte - 1) * file['RECORD_BYTES']
                with open(path, 'rb') as f:
                    f.seek(start_pos, os.SEEK_SET)
                    buf = f.read(rows*cols*rbytes)
                rownum_table = np.frombuffer(buf, dtype='>i2') & bitmask
                return np.sort(rownum_table)
        raise RuntimeError('No RowNum table found in metadata!')

    def _load_hk_table(self, path):
        """
        #TODO: parse column names and descriptions from trdrhk.fmt
        :param path:
        :return:
        """
        for _, file in (x for x in self.label if x[0] == 'FILE'):
            if self.hk_key in file:
                hk_table_file = file[self.hk_key]
                hk_table_path = Path(path).parent / hk_table_file
                return pd.read_csv(hk_table_path.absolute(), header=None, names=utils.get_trdr_hk_column_names())
        raise RuntimeError('No TRDR_HK table found!')

class FRT(CRISM):
    pass

class HRL(CRISM):
    pass

class HRS(CRISM):
    pass

class FRS(CRISM):
    pass

