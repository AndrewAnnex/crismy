from dataclasses import dataclass


@dataclass()
class FilteringParameterConfig(object):
    median_width: int = 35
    smooth_width: int = 7
    # scipy edge effect mode to use
    median_mode: str = 'nearest'
    # scipy edge effect mode to use
    smooth_mode: str = 'nearest'

@dataclass()
class NonsenseScanParameterConfig(object):
    ignore_val: float = 65535.0
    valid_range_min: float = 0.0
    valid_range_max: float = 1.0

@dataclass()
class DestripeParameterConfig(object):
    filter_config: FilteringParameterConfig = FilteringParameterConfig(median_width=15, smooth_width=5)