from crismy.filtering.filtering import Filters
from crismy.utils.logging import timed
from crismy.filtering.config import DestripeParameterConfig
import numpy as np
#from numba import jit


class StripeFilter(object):

    def __init__(self, config: DestripeParameterConfig = DestripeParameterConfig()):
        self.config = config
        self._filters = Filters(config=self.config.filter_config)
        pass

    @timed
    def destripe(self, x):
        bands, rows, cols = x.shape
        # shift right and then left,
        right_shift_x = np.roll(x, 1, axis=2)
        left_shift_x  = np.roll(x, -1, axis=2)
        # get the ratios
        right_shift_x_ratio = x / right_shift_x
        left_shift_x_ratio  = x / left_shift_x
        # get the median ratio value for
        right_shift_x_ratio_median = np.nanmedian(right_shift_x_ratio, axis=1) # should be bands, cols
        left_shift_x_ratio_median  = np.nanmedian(left_shift_x_ratio, axis=1)  # should be bands, cols
        # get the ratio trace array from the left and right median result
        ratio_trace = self._get_ratio_trace(left_shift_x_ratio_median, right_shift_x_ratio_median)
        # get the normalized ratio trace after median filtering
        ratio_trace_norm = self._get_ratio_trace_norm(ratio_trace)
        # get the rsc_map
        rsc_map = self._get_rsc_map(ratio_trace_norm)
        # get the sample_shift_ratio_trace_norm_cube which is just rsc_map repeated for each band
        sample_shift_ratio_trace_norm_cube = self._copy_for_n_rows(rsc_map, rows)
        # create the output data divided by the ratio norm cube
        out = x / sample_shift_ratio_trace_norm_cube
        return out

    @timed
    def _destripe_given_stride(self, x, stride):
        # shift spatially horizontal axis
        shift_x = np.roll(x, stride, axis=2)
        # get the ratio
        shift_x_ratio = x / shift_x
        # get the median for each row (vertical spatial axis)
        shift_x_ratio_median = np.nanmedian(shift_x_ratio, axis=1)
        return shift_x_ratio_median

    @timed
    def _get_ratio_trace(self, left_median, right_median):
        """
        Do the statistics from left to right and then right to left
        :param left_median:
        :param right_median:
        :return:
        """
        bands, samples = left_median.shape
        ratio_trace = np.ones((bands, samples, 4))
        # replicate for loops from IDL code until better understood
        for i in range(1, samples-1):
            ratio_trace[:, i, 0] = ratio_trace[:, i-1, 0] * right_median[:, i]
            ratio_trace[:, samples - 1 - i, 1] = ratio_trace[:, samples - 1 - (i - 1), 1] * right_median[:, samples - i]
        for i in range(samples -2, 0):
            ratio_trace[:, i, 2] = ratio_trace[:, i + 1, 2] * left_median[:, i]
            ratio_trace[:, samples - 1 - i, 3] = ratio_trace[:, samples - 1 - (i + 1), 3] * left_median[:, samples - 1 - (i + 1)]
        return ratio_trace

    @timed
    def _get_rsc_map(self, x):
        return np.sum(x, axis=2) / 4.0

    @timed
    def _get_ratio_trace_norm(self, ratio_trace):
        median_ratio_trace = self._median_filter(ratio_trace)
        ratio_trace_norm = ratio_trace / median_ratio_trace
        return ratio_trace_norm

    @timed
    def _copy_for_n_rows(self, x, rows):
        bands, cols = x.shape
        return np.dstack([x]*rows).reshape((bands, rows, cols))

    @timed
    def _median_filter(self, x):
        """
        Median filter for destriping, will act on the bands like a normal image filter
        :param x:
        :return:
        """
        return self._filters.spatial_median_filter(x)


if __name__ == '__main__':
    from crismy.crismy import CRISM
    import logging
    logging.basicConfig(level=logging.DEBUG)
    logger = logging.getLogger("Destripe Main Test")
    logger.setLevel(logging.DEBUG)
    f = '/Users/andrew/Dropbox/Planetary/CRISM Data/FRT0000C202/FRT0000C202_07_IF165L_TRR3/frt0000c202_07_if165l_trr3.img'
    frt = CRISM.open(f)
    logger.debug("Start")
    sf = StripeFilter()
    ds = sf.destripe(frt.data)
    logger.debug("Done")
