from scipy.ndimage import median_filter, uniform_filter1d
from crismy.filtering.config import FilteringParameterConfig
from crismy.utils.logging import timed


class Filters(object):

    def __init__(self, config: FilteringParameterConfig = FilteringParameterConfig()):
        self.config: FilteringParameterConfig = config

    @timed
    def spectral_smooth_filter_scipy(self, x):
        # important to remove band that is NAN before passing to this function
        # actually this may be a bad idea, just make sure to set 65535 to nan!
        return uniform_filter1d(x, self.config.smooth_width, axis=0, mode=self.config.smooth_mode)

    @timed
    def spectral_median_filter_scipy(self, x):
        # important to remove band that is NAN before passing to this function
        return median_filter(x, (self.config.median_width, 1, 1), mode=self.config.median_mode)

    @timed
    def spatial_median_filter(self, x):
        # important to remove band that is NAN before passing to this function
        return median_filter(x, (1, self.config.median_width, self.config.median_width), mode=self.config.median_mode)

    @staticmethod
    def spectral_smooth_filter(x, config: FilteringParameterConfig = FilteringParameterConfig()):
        return Filters(config).spectral_smooth_filter_scipy(x)

    @staticmethod
    def spectral_median_filter(x, config: FilteringParameterConfig = FilteringParameterConfig()):
        return Filters(config).spectral_median_filter_scipy(x)

