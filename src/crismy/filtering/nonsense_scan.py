from dataclasses import dataclass
from crismy.filtering.config import NonsenseScanParameterConfig
from typing import Collection
from numpy import logical_not

from crismy.utils.logging import timed


@dataclass(frozen=True)
class NonsenseScanResult(object):
    __slots__ = ['mask_ignore', 'mask_invalid_low', 'mask_invalid_high', 'mask_valid']
    """
    Masks are true when condition is true, ie in mask_ignore True values are invalid locations
    """
    mask_ignore: Collection
    mask_invalid_low: Collection
    mask_invalid_high: Collection
    mask_valid: Collection


class NonsenseScan(object):

    def __init__(self, config: NonsenseScanParameterConfig = NonsenseScanParameterConfig()):
        self.config: NonsenseScanParameterConfig = config

    @timed
    def nonsense_scan(self, x)-> NonsenseScanResult:
        mask_ignore = x == self.config.ignore_val
        mask_invalid_low = x <= self.config.valid_range_min
        mask_invalid_high = x >= self.config.valid_range_max
        mask_valid = logical_not(mask_ignore | mask_invalid_low | mask_invalid_high)
        return NonsenseScanResult(mask_ignore, mask_invalid_low, mask_invalid_high, mask_valid)

    @staticmethod
    def nonsense_scan_static(x, config: NonsenseScanParameterConfig = NonsenseScanParameterConfig())-> NonsenseScanResult:
        return NonsenseScan(config).nonsense_scan(x)