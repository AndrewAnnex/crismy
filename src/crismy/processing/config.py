from dataclasses import dataclass, field

from crismy.filtering.config import FilteringParameterConfig


@dataclass
class ProcessingConfig(object):
    """
    Overarching configuration holder for processing, will contain each sub process configuration dataclass
    used in the pipeline
    """
    filter_config: FilteringParameterConfig = field(default_factory=FilteringParameterConfig)