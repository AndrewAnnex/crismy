from crismy.processing.config import ProcessingConfig
from crismy.filtering.filtering import Filters

class Reference_Cube(object):


    def __init__(self, config: ProcessingConfig):
        self.config = config
        self.filters = Filters(self.config.filter_config)

    def reference_cube(self, in_cube):
        cube_median = self.filters.spectral_median_filter_scipy(in_cube)
        cube_smooth = self.filters.spectral_smooth_filter_scipy(in_cube)