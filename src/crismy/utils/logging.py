from functools import wraps
import logging
import time


logger = logging.getLogger(__name__)

def timed(func):
    #https://gist.github.com/bradmontgomery/bd6288f09a24c06746bbe54afe4b8a82
    """This decorator prints the execution time for the decorated function."""
    @wraps(func)
    def wrapper(*args, **kwargs):
        start = time.time()
        result = func(*args, **kwargs)
        end = time.time()
        logger.debug("{} ran in {}s".format(func.__name__, round(end - start, 2)))
        return result
    return wrapper