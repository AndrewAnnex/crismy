import numpy as np

def rows_in_a_band(d, band):
    bands, rows, cols = d.shape
    for r in range(rows):
        yield d[band, r, :]

def cols_in_a_band(d, band):
    bands, rows, cols = d.shape
    for c in range(cols):
        yield d[band, :, c]

def slices_by_rows(d):
    bands, rows, cols = d.shape
    for r in range(rows):
        yield d[:, r, :]

def slices_by_cols(d):
    bands, rows, cols = d.shape
    for c in range(cols):
        yield d[:, :, c]

def by_kernel_shape(data, rows:slice, cols:slice, bands: slice):
    return data[bands,rows,cols]