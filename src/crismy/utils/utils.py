import matplotlib.pyplot as plt
import pvl
import numpy as np
import os
from skimage import exposure
from ipywidgets import *

trdrhk = os.path.join(os.path.dirname(__file__), 'data/trdrhk.fmt')

def is_odd(x):
    return bool(x & 1)

def is_even(x):
    return not is_odd(x)

def get_trdr_hk_column_names(trdrhk_overide=None):
    fmt = pvl.load(trdrhk if not trdrhk_overide else trdrhk_overide)
    return [x[1]['NAME'] for x in fmt]

def normalize_array(x):
    return x / x.max(axis=0).max(axis=0)

def transpose_hsp_data(hsp):
    return np.transpose(hsp, axes=[1,2,0])

def select_subset(hsp, b1, b2 = None, b3 = None, normalize=True):
    if b2 and b3:
        view = transpose_hsp_data(hsp[[b1, b2, b3], :, :])
    else:
        view = transpose_hsp_data(hsp[[b1], :, :])
    if normalize:
        view = normalize_array(view)
    return view

def view_hsp(hsp, b1, b2 = None, b3 = None, rows: int = 1, cols: int = 6, figsize=(30,40)):
    view = select_subset(hsp, b1, b2=b2, b3=b3)
    if rows != 1 and cols != 1:
        raise RuntimeError("Error, I don't currently plot that way, either rows or cols must equal 1!")
    # get the number of subplots
    step_size = hsp.lines // (cols if cols > rows else rows)
    # get the subplots
    fig, axes = plt.subplots(nrows=rows, ncols=cols, squeeze=False, figsize=figsize)
    sideways  = rows > cols
    for row in range(rows):
        for col in range(cols):
            start = (row+col) * step_size
            end   = start + step_size - 1
            indices = (slice(start,end), ...) if b2 and b3 else (slice(start,end), ..., 0)
            subview = np.rot90(view[indices]) if sideways else view[indices]
            #subview = view[indices]
            if b2 and b3:
                axes[row,col].imshow(subview)
            else:
                axes[row,col].imshow(subview, cmap='gray')
    plt.show()
    return fig, axes

def view_hsp_v(hsp, b1, b2 = None, b3 = None, cols: int = 6, figsize=(30,40)):
    return view_hsp(hsp, b1, b2=b2, b3=b3, rows=1, cols=cols, figsize=figsize)

def view_hsp_h(hsp, b1, b2 = None, b3 = None, rows: int = 6, figsize=(30,40)):
    return view_hsp(hsp, b1, b2=b2, b3=b3, rows=rows, cols=1, figsize=figsize)

def view_hsp_old(hsp, b1, b2 = None, b3 = None, cols: int = 6, figsize=(30,40)):
    # TODO: make more better
    if b2 and b3:
        view = np.transpose(hsp[[b1, b2, b3], :, :], axes=[1, 2, 0])
        view /= view.max(axis=0).max(axis=0)
    else:
        view = np.transpose(hsp[b1, :, :][np.newaxis, :], axes=[1, 2, 0])
    step_size = hsp.lines // cols
    fig, axes = plt.subplots(1, cols, figsize=figsize)
    try:
        if hasattr(axes, '__len__'):
            for i, ax in enumerate(axes):
                start = i*step_size
                end   = start + step_size - 1
                if b2 and b3:
                    ax.imshow(view[start:end, :, :])
                else:
                    ax.imshow(view[start:end, :, 0], cmap='gray')
        else:
            if b2 and b3:
                axes.imshow(view)
            else:
                axes.imshow(view[:,:,0], cmap='gray')
    finally:
        plt.show()
    return fig, axes

def get_bounded_sliders(bands, R, G=None, B=None):
    Rs = BoundedIntText(value=R, min=0, max=bands, step=1, continuous_update=False)
    if G is None or B is None:
        return Rs
    else:
        Gs = BoundedIntText(value=G, min=0, max=bands, step=1, continuous_update=False)
        Bs = BoundedIntText(value=B, min=0, max=bands, step=1, continuous_update=False)
        return Rs, Gs, Bs

def get_sliders(bands, R, G=None, B=None):
    Rs = IntSlider(value=R, min=0, max=bands, step=1, continuous_update=False)
    if G is None or B is None:
        return Rs
    else:
        Gs = IntSlider(value=G, min=0, max=bands, step=1, continuous_update=False)
        Bs = IntSlider(value=B, min=0, max=bands, step=1, continuous_update=False)
        return Rs, Gs, Bs

def widget_RGB(data, normalization):
    def _widget(R, G, B):
        subset = normalization(data[:, :, [R, G, B]])
        #plt.figure(figsize=(3, 10))
        plot = plt.imshow(subset)
        plt.title(f'Bands R: {R}, G: {G}, B: {B}')
    return _widget

def widget_Gray(data, normalization):
    def _widget(R):
        subset = normalization(data[:, :, R])
        #plt.figure(figsize=(3, 10))
        plot = plt.imshow(subset, cmap='gray')
        plt.title(f'Band Gray: {R}')
    return _widget

def plot_widget_RGB(hsp, R, G, B, x=slice(0, 200), y=slice(3, -1), use_selector=False):
    data = transpose_hsp_data(hsp.data[:, x, y])
    _widget = widget_RGB(data, normalize_array)
    Rs, Gs, Bs = get_bounded_sliders(hsp.bands, R, G, B) if use_selector else get_sliders(hsp.bands, R, G, B)
    return interactive(_widget, R=Rs, G=Gs, B=Bs)

def plot_widget_Gray(hsp, R, x=slice(0,200), y=slice(3,-1), use_selector=False):
    data  = transpose_hsp_data(hsp.data[:, x, y])
    _widget = widget_Gray(data, normalize_array)
    Rs = get_bounded_sliders(hsp.bands, R) if use_selector else get_sliders(hsp.bands, R)
    return interactive(_widget, R=Rs)

def plot_widget_RGB_equalized(hsp, R, G, B, x=slice(0, 200), y=slice(3, -1), use_selector=False):
    data = transpose_hsp_data(hsp.data[:, x, y])
    _widget = widget_RGB(data, exposure.equalize_hist)
    Rs, Gs, Bs = get_bounded_sliders(hsp.bands, R, G, B) if use_selector else get_sliders(hsp.bands, R, G, B)
    return interactive(_widget, R=Rs, G=Gs, B=Bs)

def plot_widget_Gray_equalized(hsp, R, x=slice(0,200), y=slice(3,-1), use_selector=False):
    data  = transpose_hsp_data(hsp.data[:, x, y])
    _widget = widget_Gray(data, exposure.equalize_hist)
    Rs = get_bounded_sliders(hsp.bands, R) if use_selector else get_sliders(hsp.bands, R)
    return interactive(_widget, R=Rs)

def plot_widget_RGB_stretched(hsp, R, G, B, x=slice(0,200), y=slice(3,-1), use_selector=False):
    data = transpose_hsp_data(hsp.data[:, x, y])
    _widget = widget_RGB(data, exposure.rescale_intensity)
    Rs, Gs, Bs = get_bounded_sliders(hsp.bands, R, G, B) if use_selector else get_sliders(hsp.bands, R, G, B)
    return interactive(_widget, R=Rs, G=Gs, B=Bs)

def plot_widget_Gray_stretched(hsp, R, x=slice(0,200), y=slice(3,-1), use_selector=False):
    data = transpose_hsp_data(hsp.data[:, x, y])
    _widget = widget_Gray(data, exposure.rescale_intensity)
    Rs = get_bounded_sliders(hsp.bands, R) if use_selector else get_sliders(hsp.bands, R)
    return interactive(_widget, R=Rs)

